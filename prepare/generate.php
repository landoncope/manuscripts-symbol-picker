<?php

$tmpDir = __DIR__ . '/tmp';

if (!file_exists($tmpDir)) {
    mkdir($tmpDir);
}

$blocksFile = "$tmpDir/blocks.json";

if (!file_exists($blocksFile)) {
    $url = 'http://unicode.org/Public/UCD/latest/ucdxml/ucd.nounihan.flat.zip';
    $zipFile = "$tmpDir/ucd.nounihan.flat.zip";
    print "Fetching $url\n";
    copy($url, $zipFile);

    $zip = new ZipArchive();
    $zip->open($zipFile);

    $xml = $zip->getFromName('ucd.nounihan.flat.xml');
    $doc = new \DOMDocument();
    $doc->loadXML($xml);

    $xpath = new \DOMXPath($doc);
    $xpath->registerNamespace('ucd', 'http://www.unicode.org/ns/2003/ucd/1.0');

    $blocks = [];

    /** @var \DOMElement $block */
    foreach ($xpath->query('ucd:blocks/ucd:block') as $block) {
        $blocks[] = [
            'name' => $block->getAttribute('name'),
            'from' => $block->getAttribute('first-cp'),
            'to' => $block->getAttribute('last-cp'),
        ];
    }

    file_put_contents($blocksFile, json_encode($blocks, JSON_PRETTY_PRINT));
}

$blocks = json_decode(file_get_contents($blocksFile), true);

$categories = [
  IntlChar::CHAR_CATEGORY_CURRENCY_SYMBOL,
  IntlChar::CHAR_CATEGORY_MATH_SYMBOL,
  IntlChar::CHAR_CATEGORY_MODIFIER_SYMBOL,
  IntlChar::CHAR_CATEGORY_OTHER_SYMBOL,
];

$grouped = [];

foreach ($blocks as $index => $block) {
    $blockName = $block['name'];

    $items = [];

    $from = hexdec($block['from']);
    $to = hexdec($block['to']);

    for ($codepoint = $from; $codepoint <= $to; $codepoint++) {
        $name = IntlChar::charName($codepoint, IntlChar::EXTENDED_CHAR_NAME);
        $category = IntlChar::charType($codepoint);

        if (in_array($category, $categories)) {
            $character = IntlChar::chr($codepoint);
            $entity = htmlentities($character, ENT_QUOTES, 'UTF-8');
            $ampersandEntity = preg_match('/^&.+;$/', $entity) ? $entity : '';
//            $alias = IntlChar::charName($codepoint, IntlChar::CHAR_NAME_ALIAS);
            $categoryName = IntlChar::getPropertyValueName(IntlChar::PROPERTY_GENERAL_CATEGORY, $category);

            $items[] = [
                $character,
                $ampersandEntity,
                $codepoint,
                $name,
                preg_replace('/_/', ' ', $categoryName),
//                $alias
            ];
        }
    }

    if (!empty($items)) {
      $grouped[$blockName] = $items;
    }
}

file_put_contents(__DIR__ . '/../src/grouped.json', json_encode($grouped));

