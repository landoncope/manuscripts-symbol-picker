module.exports = {
  plugins: [
    '@babel/proposal-object-rest-spread',
    '@babel/syntax-dynamic-import',
  ],
  presets: [
    '@babel/env',
    '@babel/react',
    '@babel/typescript',
  ],
}
