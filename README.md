# @manuscripts/symbol-picker

A React component for picking a Unicode character.

# Install

`yarn add @manuscripts/symbol-picker`

# Usage

```
import { SymbolPicker } from '@manuscripts/symbol-picker'

<SymbolPicker handleSelectCharacter={…} />
```

# Character data

The character data is derived from a combination of the block ranges in the official Unicode data and the character information built into PHP.

To regenerate the `grouped.json` file that contains the list of characters in each block, run `php prepare/generate.php`. Remove the cached data in `prepare/tmp`  to fetch updated data if needed.
